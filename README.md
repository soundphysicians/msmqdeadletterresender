# MSMQ Dead Letter Resender

Command-line utility for processing NServiceBus messages in the transactional dead letter queue, removing items that do not meet requirements and forwarding messages to the original destination queues.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
.NET Framework 4.6.2
MSMQ
```

### Executing

A step by step series of examples that tell you have to get a development env running

Running with default parameters. Default is to resend 30 days worth of messages and to assume the serviceControl queue is available at 'particular.servicecontrol'

```
MsmqDeadLetterResender.exe -m <machineName>
```

Executing against the transactional dead letter queue on `<machineName>` and resending only messages sent within the past 7 days. This option specifies that the serviceControl queue is 'particular.serviceControl2' and that we want to ignore messages sent to the audit queue.

```
MsmqDeadLetterResender.exe -m <machineName> -r 7 -s particular.servicecontrol2 -i audit
```

## Built With

* [.NET Framework](https://docs.microsoft.com/en-us/dotnet/framework/) - Application framework

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Steve Bering** - *Initial work*

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details
