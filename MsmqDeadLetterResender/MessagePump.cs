﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Transactions;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Serilog;

namespace MsmqDeadLetterResender
{
    public static class MessagePump
    {
        private const string MessageRemovedLogTemplate = "Removing message {MessageId}. Reason: '{ReasonForDeletion}'.";
        private const string NservicebusTimesentKey = "NServiceBus.TimeSent";
        private static readonly ILogger Logger = Log.ForContext(typeof(MessagePump));

        private static DateTime _discardAllToDate;
        private static DeadLetterResenderOptions _options;

        public static void Start(DeadLetterResenderOptions options)
        {
            _discardAllToDate = DateTime.Now.Subtract(TimeSpan.FromDays(options.RetentionPeriodInDays));
            _options = options;

            var queue = new MessageQueue(options.DeadLetterQueue, QueueAccessMode.SendAndReceive);
            queue.MessageReadPropertyFilter.SetAll();

            // add an event handler that fires every time we receive a message
            queue.ReceiveCompleted += MsmqBridgeOnReceiveCompleted;

            // begin the async receive operation
            queue.BeginReceive();

            Console.WriteLine($"Watching MSMQ: {options.DeadLetterQueue} for messages. " +
                              "Received messages that meet filtering requirement will be sent to original destination.");
            Console.WriteLine("Press any key to quit.");
            Console.ReadKey();
        }

        static void MsmqBridgeOnReceiveCompleted(object sender, ReceiveCompletedEventArgs receiveCompletedEventArgs)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                // connect to the queue
                var messageQueue = (MessageQueue)sender;

                // end the async receive operation
                var message = messageQueue.EndReceive(receiveCompletedEventArgs.AsyncResult);

                // validate the message 
                if (message != null)
                {
                    Logger.Debug("Received a message in MSMQ - Processing");

                    // serialize headers to dictionary 
                    var headerString = Encoding.UTF8.GetString(message.Extension).TrimEnd('\0');
                    var headers = ExtractHeaders(headerString);

                    // we have what we need now, determine if we are going to send it or not. 
                    // If we want to forward it, do so
                    if (ShouldForward(message, headers))
                    {
                        Logger.Information("Forwarding message {MessageId}...", message.Id);
                        throw new ApplicationException("Forwarding is not yet tested.");
                        ForwardMessage(message, headers);
                        Logger.Information("Forwarding... DONE");
                    }
                    else
                    {
                        Logger.Debug("Message {MessageId} Ignored. Message has been removed from queue.",
                            message.Id);
                    }

                    Logger.Debug("Received a message in MSMQ - Processing Complete\n");
                }

                // Restart the asynchronous receive operation.
                messageQueue.BeginReceive();

                // Commit transaction
                scope.Complete();
            }
        }

        private static void ForwardMessage(Message message, Dictionary<string, string> headers)
        {
            var json = JsonConvert.SerializeObject(headers);
            Logger.Debug("Message headers: {Headers}", json);

            var destinationQueueName = message.DestinationQueue?.Path;
            if (destinationQueueName != null)
            {
                var destinationQueue = new MessageQueue(destinationQueueName, QueueAccessMode.Send);
                destinationQueue.Send(message, MessageQueueTransactionType.Automatic);
            }
        }

        private static bool ShouldForward(Message message, Dictionary<string, string> headers)
        {
            LogMessage(message, headers);

            if (IsQueuePurged(message))
            {
                Logger.Debug(MessageRemovedLogTemplate, message.Id, "Purged Queue");
                return false;
            }

            if (IsHeartbeatMessage(message))
            {
                Logger.Debug(MessageRemovedLogTemplate, message.Id, "Heartbeat Message");
                return false;
            }

            if (IsIgnoredQueue(message))
            {
                Logger.Debug(MessageRemovedLogTemplate, message.Id, "Ignored Queue");
                return false;
            }

            if (IsExpiredMessage(message, headers))
            {
                Logger.Debug(MessageRemovedLogTemplate, message.Id, "Expired Message");
                return false;
            }

            return true;
        }

        private static bool IsQueuePurged(Message message)
        {
            if (message.Acknowledgment == Acknowledgment.QueuePurged)
            {
                return true;
            }

            return false;
        }

        private static void LogMessage(Message message, Dictionary<string, string> headers)
        {
            var queue = message.DestinationQueue?.FormatName;
            var id = message.Id;
            var timeSent = GetTimeSent(message, headers);

            Logger.Debug("Processing message {MessageId} sent {TimeSent} to {DestinationQueue}...", id, timeSent.ToShortDateString(), queue);
        }

        private static bool IsIgnoredQueue(Message message)
        {
            var formatName = message.DestinationQueue?.FormatName ?? "";
            var ignoredQueues = _options.IgnoredQueues ?? new List<string>();
            foreach (var queue in ignoredQueues)
            {
                if (formatName.EndsWith(queue, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsExpiredMessage(Message message, Dictionary<string, string> headers)
        {
            var timeSent = GetTimeSent(message, headers);
            // throw away messages older than the requested discard date
            return timeSent < _discardAllToDate;
        }

        private static DateTime GetTimeSent(Message message, Dictionary<string, string> headers)
        {
            const string format = "yyyy-MM-dd HH:mm:ss:ffffff Z";

            if (!headers.ContainsKey(NservicebusTimesentKey))
            {
                return message.SentTime;
            }

            var timeSentHeader = headers[NservicebusTimesentKey];
            if (timeSentHeader != null)
            {
                return DateTime.ParseExact(timeSentHeader, format, CultureInfo.InvariantCulture).ToUniversalTime();
            }

            return message.SentTime;
        }

        private static Dictionary<string, string> ExtractHeaders(string headerString)
        {
            // Deserialize the XML stream from the Extension property on the MSMQ
            // to a dictionary of key-value pairs
            var headerSerializer = new XmlSerializer(typeof(List<HeaderInfo>));
            var readerSettings = new XmlReaderSettings
            {
                CheckCharacters = false
            };
            using (var stream = new StringReader(headerString))
            using (var reader = XmlReader.Create(stream, readerSettings))
            {
                var headers = (List<HeaderInfo>)headerSerializer.Deserialize(reader);
                return headers
                    .Where(pair => pair.Key != null)
                    .ToDictionary(pair => pair.Key, pair => pair.Value);
            }
        }

        private static bool IsHeartbeatMessage(Message message)
        {
            var formatName = message.DestinationQueue?.FormatName;
            if ((formatName ?? "").EndsWith(_options.ServiceControlQueueName, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }
    }

    [Serializable]
    public class HeaderInfo
    {
        // The key used to lookup the value in the header collection.
        public string Key { get; set; }

        // The value stored under the key in the header collection.
        public string Value { get; set; }
    }
}