﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.CommandLineUtils;
using Serilog;
using Serilog.Events;

namespace MsmqDeadLetterResender
{
    class Program
    {
        private static class OptionNames
        {
            public const string MachineName = "machineName";
            public const string ServiceControlQueueName = "serviceControlQueueName";
            public const string RetentionInDays = "retentionInDays";
            public const string IgnoredQueues = "ignoredQueues";
        }

        static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.ColoredConsole(restrictedToMinimumLevel: LogEventLevel.Information)
                .WriteTo.RollingFile("log-{Date}.txt", restrictedToMinimumLevel: LogEventLevel.Information)
                .CreateLogger();

            var app = new CommandLineApplication(false)
            {
                Name = "MsmqDeadLetterResender.exe",
                Description =
                    "Processes messages in the Transactional Dead Letter Queue on the specified machine. " +
                    "If messages are in any of the following categories, they will be permanently removed:" +
                    "\n    - Purged queue" +
                    "\n    - Healthcheck messages" +
                    "\n    - Expired messages (older than 30 days)",
                ShowInHelpText = true,
            };

            var hostOption = app.Option("-m | --machine <machineName>", "The name of the machine hosting the Transactional Dead Letter Queue.", CommandOptionType.SingleValue);
            hostOption.SymbolName = OptionNames.MachineName;

            var servicePulseOption = app.Option("-s | --service-control <queueName>",
                "The name of the service control queue. Used to determine if the message is a heartbeat message. Defaults to 'particular.servicecontrol'.",
                CommandOptionType.SingleValue);
            servicePulseOption.SymbolName = OptionNames.ServiceControlQueueName;

            var retentionPeriodOption = app.Option("-r | --retention <days>",
                "Number of days to retain messages. Messages older than this time will be purged, newer messages will be retried.",
                CommandOptionType.SingleValue);
            retentionPeriodOption.SymbolName = OptionNames.RetentionInDays;

            var ignoredQueuesOption = new CommandOption("-i | --ignore <queueName>", CommandOptionType.MultipleValue)
            {
                Description = "Destination queues to ignore.",
                SymbolName = OptionNames.IgnoredQueues
            };
            app.Options.Add(ignoredQueuesOption);

            app.HelpOption("-? | -h | --help");

            app.OnExecute(() =>
            {
                if (hostOption.HasValue() && !string.IsNullOrWhiteSpace(hostOption.Value()))
                {
                    var options = BuildOptions(app.Options);
                    MessagePump.Start(options);
                }
                else
                {
                    app.ShowHelp(hostOption.LongName);
                }

                return 0;
            });

            app.Execute(args);
        }

        private static string GetTransactionDeadLetterQueuePath(string hostName)
        {
            return $@"formatname:DIRECT=OS:{hostName}\SYSTEM$;DEADXACT";
        }

        private static DeadLetterResenderOptions BuildOptions(List<CommandOption> appOptions)
        {
            var hostNameOption = appOptions.First(x => x.SymbolName == OptionNames.MachineName);
            var hostName = hostNameOption.Value();
            var queueName = GetTransactionDeadLetterQueuePath(hostName);

            var options = new DeadLetterResenderOptions(queueName);

            var retentionOption = appOptions.First(x => x.SymbolName == OptionNames.RetentionInDays);
            if (retentionOption.HasValue() && int.TryParse(retentionOption.Value(), out var retention))
            {
                options.RetentionPeriodInDays = retention;
            }

            var serviceControlOption = appOptions.First(x => x.SymbolName == OptionNames.ServiceControlQueueName);
            if (serviceControlOption.HasValue())
            {
                options.ServiceControlQueueName = serviceControlOption.Value();
            }

            var ignoredQueuesOption = appOptions.First(x => x.SymbolName == OptionNames.IgnoredQueues);
            if (ignoredQueuesOption.HasValue())
            {
                options.IgnoredQueues = ignoredQueuesOption.Values;
            }

            return options;
        }
    }

    public class DeadLetterResenderOptions
    {
        private const int DefaultRetentionPeriod = 30;
        private const string DefaultServiceControlQueueName = "particular.servicecontrol";

        private int? _retentionPeriodInDays;
        private string _serviceControlQueueName;
        private IList<string> _ignoredQueues;

        public int RetentionPeriodInDays {
            get => this._retentionPeriodInDays ?? DefaultRetentionPeriod;
            set => this._retentionPeriodInDays = value;
        }

        public string ServiceControlQueueName {
            get => this._serviceControlQueueName ?? DefaultServiceControlQueueName;
            set => this._serviceControlQueueName = value;
        }

        public IList<string> IgnoredQueues {
            get => this._ignoredQueues ?? new List<string>();
            set => this._ignoredQueues = value;
        }

        public string DeadLetterQueue { get; }

        public DeadLetterResenderOptions(string deadLetterQueue, int? retentionPeriodInDays = default(int?), string serviceControlQueueName = null)
        {
            DeadLetterQueue = deadLetterQueue;
            _retentionPeriodInDays = retentionPeriodInDays;
            _serviceControlQueueName = serviceControlQueueName;
        }
    }
}
